# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Fixed
- Response when a users is unauthenticated or fails authentication.

### Added
- Command line parameters to specify debugging and config file path

## 0.1.0 - 2018-08-26
### Added
- Initial release of pnut-gitlab-bot
- webhooks for push, issue, tag, merge request, and wiki events
- bot commands for join, part, webhook, login, logout, create, comment, and close

[Unreleased]: https://gitlab.dreamfall.space/thrrgilag/pnut-gitlab-bot/compare/v0.1.0...HEAD
