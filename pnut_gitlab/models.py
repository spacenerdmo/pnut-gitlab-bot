import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class BotUser(Base):
    __tablename__ = 'bot_user'
    id = Column(Integer, primary_key=True)
    pnut_user = Column(String(250), unique=True)
    pnut_id = Column(Integer, unique=True)
    gitlab_token = Column(String(250))

class BotChannel(Base):
    __tablename__ = 'bot_channel'
    id = Column(Integer, primary_key=True)
    channel_id = Column(Integer, unique=True)
    channel_type = Column(String(250))
    channel_owner = Column(Integer)
    channel_admins = Column(String(500))
    secret = Column(String(128))
