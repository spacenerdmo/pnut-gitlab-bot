import logging
import pnutpy
import yaml
from flask import Flask, jsonify, request, abort
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from pnut_gitlab.models import *

app = Flask(__name__)

# setup the environment
SNAP_DATA = os.environ.get('SNAP_DATA')
if SNAP_DATA is None:
    filename = 'config.yaml'
    db_url = 'sqlite:////store.db'
    log_file = 'webhook.log'
else:
    filename = SNAP_DATA + '/config.yaml'
    db_url = 'sqlite:///' + SNAP_DATA + '/store.db'
    log_file = SNAP_DATA + '/webhook.log'

logging.basicConfig(level=logging.DEBUG, filename=log_file)
with open(filename, "rb") as config_file:
    config = yaml.load(config_file, Loader=yaml.SafeLoader)

pnutpy.api.add_authorization_token(config['bot_token'])

# setup the database connection
engine = create_engine(db_url)
Base.metadata.create_all(engine)
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


@app.route("/webhooks/gitlab/<int:channel>", methods=['POST'])
def gitlab_event(channel=None):
    token = request.headers['X-Gitlab-Token']
    event = request.headers['X-Gitlab-Event']
    logging.debug(request.headers)

    try:
        botchannel = session.query(BotChannel).filter(BotChannel.channel_id == channel).one()

        if botchannel.secret == token:

            if event == 'Push Hook':
                gitlab_push_event(channel, request.json)

            elif event == 'Issue Hook':
                gitlab_issue_event(channel, request.json)

            elif event == 'Tag Push Hook':
                gitlab_tag_event(channel, request.json)

            elif event == 'Note Hook':
                gitlab_note_event(channel, request.json)

            elif event == 'Merge Request Hook':
                gitlab_merge_event(channel, request.json)

            elif event == 'Wiki Page Hook':
                gitlab_wiki_event(channel, request.json)

        else:
            logging.debug('INVALID TOKEN')
            abort(401)

    except NoResultFound:
        logging.debug('INVALID CHANNEL')
        abort(404)

    return '', 200


def gitlab_push_event(channel, data):

    if data['total_commits_count'] > 1:
        plural = 's'
    else:
        plural = ''

    branch = data['ref'][len('refs/heads/'):]

    header = "[{0}/{1}|{2}] {3} new commit{4} by {5}\n".format(
        data['project']['namespace'], # 0
        data['project']['name'],      # 1
        branch,                       # 2
        data['total_commits_count'],  # 3
        plural,                       # 4
        data['user_username'])        # 5

    body = ""
    for commit in data['commits']:
        message = commit['message'].strip()
        body += "[{0}]({1}) - {2}".format(
            commit['id'][:8],  #0
            commit['url'],     #1
            message)           #2
        body += "\n"

    msg = header + "\n" + body
    logging.debug(msg)
    pnutpy.api.create_message(channel, data={'text': msg})

def gitlab_issue_event(channel, data):
    logging.debug(data)

    if not 'action' in data['object_attributes']:
        return

    action = data['object_attributes']['action']
    if action == 'update' or len(action) == 0:
        return

    if not action.endswith('e'):
        action += 'e'

    header = "[{0}/{1}] {2} {3}d issue [#{4}]({5})\n{6}".format(
        data['project']['namespace'],
        data['project']['name'],
        data['user']['username'],
        action,
        data['object_attributes']['iid'],
        data['object_attributes']['url'],
        data['object_attributes']['title'])

    logging.debug(header)
    pnutpy.api.create_message(channel, data={'text': header})

def gitlab_tag_event(channel, data):

    tag = data['ref'][len('refs/tags/'):]

    header = "[{0}/{1}] {2} created tag [{3}]({4}/tags/{3})".format(
        data['project']['namespace'], # 0
        data['project']['name'],      # 1
        data['user_username'],        # 2
        tag,                          # 3
        data['project']['web_url'])   # 4

    logging.debug(header)
    pnutpy.api.create_message(channel, data={'text': header})

def gitlab_note_event(channel, data):

    notetype = data['object_attributes']['noteable_type']

    if notetype == 'Commit':
        noteid = data['object_attributes']['commit_id'][:8]
        prefix = ""

    elif notetype == 'MergeRequest':
        noteid = data['merge_request']['iid']
        prefix = "!"

    elif notetype == 'Issue':
        noteid = data['issue']['iid']
        prefix = "#"

    elif notetype == 'Snippet':
        noteid = data['snippet']['id']
        prefix = ""

    else:
        noteid = ""
        prefix = ""
        return

    header = "[{0}/{1}] {2} commented on {3} [{6}{4}]({5})".format(
        data['project']['namespace'],      # 0
        data['project']['name'],           # 1
        data['user']['username'],          # 2
        notetype,                          # 3
        noteid,                            # 4
        data['object_attributes']['url'],  # 5
        prefix)                            # 6

    body = "{0}".format(
        data['object_attributes']['note'])

    msg = header + "\n\n" + body
    logging.debug(msg)
    pnutpy.api.create_message(channel, data={'text': msg})

def gitlab_merge_event(channel, data):

    if not 'action' in data['object_attributes']:
        return

    action = data['object_attributes']['action']
    if action == 'update' or len(action) == 0:
        return

    if not action.endswith('e'):
        action += 'e'

    header = "[{0}/{1}] {2} {3}d merge request [!{4}]({5})\n{6}".format(
        data['project']['namespace'],
        data['project']['name'],
        data['user']['username'],
        action,
        data['object_attributes']['iid'],
        data['object_attributes']['url'],
        data['object_attributes']['title'])

    logging.debug(header)
    pnutpy.api.create_message(channel, data={'text': header})

def gitlab_wiki_event(channel, data):

    if not 'action' in data['object_attributes']:
        return

    action = data['object_attributes']['action']

    if not action.endswith('e'):
        action += 'e'

    header = "[{0}/{1}] {2} {3}d wiki page [{4}]({5})\n{6}".format(
        data['project']['namespace'],
        data['project']['name'],
        data['user']['username'],
        action,
        data['object_attributes']['title'],
        data['object_attributes']['url'],
        data['object_attributes']['message'])

    logging.debug(header)
    pnutpy.api.create_message(channel, data={'text': header})


if __name__ == '__main__':

    logging.info('--- starting ---')
    app.run(host='0.0.0.0', port=4558)
