import pnutpy
import logging
import websocket
import time
import json
import gitlab
import yaml
import secrets
import string
import _thread
import argparse

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from pnut_gitlab.models import *

SNAP_DATA = os.environ.get('SNAP_DATA')

class PnutBot:

    def __init__(self, config, session):
        self.config = config
        self.session = session
        self.api = pnutpy.api
        self.api.add_authorization_token(self.config['bot_token'])
        websocket.enableTrace(False)
        self.ws = websocket.WebSocketApp(self.config['ws_url'],
                              on_message = self.on_message,
                              on_error = self.on_error,
                              on_close = self.on_close)
        self.ws.on_open = self.on_open

    def on_error(self, ws, error):
        logging.debug('*** error ***')
        logging.debug(error)

    def on_close(self, ws, status_code, msg):
        logging.debug("### closed ###")
        logging.debug(status_code)
        logging.debug(msg)

    def on_open(self, ws):

        def run(*args):
            while True:
                ws.send(".")
                time.sleep(30)
            logging.debug("*** thread terminating ***")

        self.get_subscribed()
        _thread.start_new_thread(run, ())

    def on_message(self, ws, message):
        msg = json.loads(message)
        if 'meta' in msg:
            meta = msg['meta']
            # logging.debug(meta.keys())
        else:
            logging.debug('*** unknown server message ***')
            logging.debug(message)
            return

        if 'data' in msg and 'channel_type' in meta:
            botchannels = self.session.query(BotChannel).all()
            logging.debug(meta['channel_type'])
            for mdata in msg['data']:
                if meta['channel_type'] == 'io.pnut.core.chat' and 'content' in mdata:
                    logging.debug('--chat--')
                    if mdata['content']['text'].startswith('@gitlab '):
                        self.bot_command(mdata, True)
                elif meta['channel_type'] == 'io.pnut.core.pm' and 'content' in mdata:
                    logging.debug('--pm--')
                    if int(mdata['channel_id']) in [bc.channel_id for bc in botchannels]:
                        self.bot_command(mdata, False)

        elif 'data' in msg and meta['type'] == 'post':
            # logging.debug('*** post ***')
            pass

        elif 'type' in meta and meta['type'] == 'channel_subscription':
            logging.debug('*** channel subscribe ***')
            self.subscription(meta['id'])

        else:
            logging.debug(msg)

    def bot_command(self, msg, public=False):
        channel_id = msg['channel_id']

        # ignore replies from the gitlab bot
        if msg['user']['username'] == self.config['bot_username']:
            return

        try:
            botuser = self.session.query(BotUser).filter(BotUser.pnut_id  == msg['user']['id']).one()
        except NoResultFound:
            botuser = BotUser(
                pnut_id=msg['user']['id'],
                pnut_user=msg['user']['username'])
            self.session.add(botuser)
            self.session.commit()

        if public:
            p, cmd = msg['content']['text'].split(' ', 1)
            if p != '@gitlab':
                return
            argv = cmd.split(' ', 1)
        else:
            argv = msg['content']['text'].split(' ', 1)

        action = argv[0]
        if len(argv) > 1:
            args = argv[1]
        else:
            args = ""

        if action.lower() == 'help':
            self.cmd_help(channel_id, public)

        elif action.lower() == 'ping':
            self.cmd_ping(channel_id)

        elif action.lower() == 'join' and not public:
            self.cmd_join(channel_id, msg['user'], args)

        elif action.lower() == 'part' and not public:
            self.cmd_part(channel_id, msg['user'], args)

        elif action.lower() == 'subscribed' and not public:
            self.cmd_subscribed(channel_id, msg['user'])

        elif action.lower() == 'whoami':
            self.cmd_whoami(channel_id, botuser.gitlab_token)

        elif action.lower() == 'login' and not public:
            self.cmd_login(channel_id, botuser, args)

        elif action.lower() == 'logout' and not public:
            self.cmd_logout(channel_id, botuser)

        elif action.lower() == 'webhook' and not public:
            self.cmd_get_webhook(channel_id, msg['user'], args)

        elif action.lower() == 'create':
            self.cmd_create(channel_id, botuser.gitlab_token, args)

        elif action.lower() == 'close':
            self.cmd_close(channel_id, botuser.gitlab_token, args)

        elif action.lower() == 'comment':
            self.cmd_comment(channel_id, botuser.gitlab_token, args)

    def cmd_help(self, channel_id, public):
        commands = ""
        commands += "whoami -- Show who you are logged in as on gitlab.com\n"
        commands += "create <project> <title> -- Create an issue. Issue description can be placed on a new line.\n"
        commands += "close <project> <id> -- Close an issue.\n"
        commands += "comment <project> <id> <comment> -- Comment on an issue.\n"

        if public:
            title = "The following commands are prefixed with @gitlab \n\n"
        else:
            title = "The following commands can be given directly in this private chat\n\n"
            commands += "login <token> -- Login to gitlab using a private access token.\n"
            commands += "logout -- Logout of gitlab by removing the access token.\n"
            commands += "join <id> -- Add the bot to the specified pnut channel.\n"
            commands += "part <id> -- Remove the bot from the specified pnut channel.\n"
            commands += "webhook <id> -- Get the webhook URL and secret for the specified pnut channel.\n"

        reply = title + commands + "\n"
        self.api.create_message(channel_id, data={'text': reply})

    def cmd_ping(self, channel):
        reply = "pong"
        self.api.create_message(channel, data={'text': reply})

    def cmd_whoami(self, channel, token):
        gl = gitlab.Gitlab('https://gitlab.com', private_token=token)

        try:
            gl.auth()
            reply = "You are currently authenticated as {0} on gitlab.com".format(gl.user.username)

        except gitlab.exceptions.GitlabAuthenticationError:
            reply = "You currently do not have an access token set for gitlab.com.\n"
            reply += "Send me a PM with the command 'help' for more details."

        except gitlab.exceptions.GitlabHttpError:
            reply = "Some unknown error has occured.\n"
            reply += "Please let @thrrgilag know."

        self.api.create_message(channel, data={'text': reply})

    def cmd_login(self, channel_id, user, args):

        if len(args) < 1:
            reply = "You must supply an access token to login"
            self.api.create_message(channel_id, data={'text': reply})
            return

        gl = gitlab.Gitlab('https://gitlab.com', private_token=args)

        try:
            gl.auth()
            reply = "You are currently authenticated as {0} on gitlab.com".format(gl.user.username)
            user.gitlab_token = args
            self.session.commit()

        except gitlab.exceptions.GitlabAuthenticationError:
            reply = "I am unable to validate your token. Please try again with a new token."
            self.session.rollback()

        except gitlab.exceptions.GitlabHttpError:
            reply = "Some unknown error has occured.\n"
            reply += "Please let @thrrgilag know."
            self.session.rollback()

        self.api.create_message(channel_id, data={'text': reply})

    def cmd_logout(self, channel_id, user):
        user.gitlab_token = ''
        self.session.commit()
        reply = "You have been logged out of gitlab.com"
        self.api.create_message(channel_id, data={'text': reply})

    def cmd_join(self, channel_id, pnut_user, args):
        reply = ""
        
        channel, meta = self.api.get_channel(args)

        if channel.type == 'io.pnut.core.chat':

            if not channel.you_subscribed:

                sc, sm = self.api.subscribe_channel(args)
                if sc.you_subscribed:
                    reply = "I am now subscribed to this channel"

            else:
                reply = "I appear to be already subscribed to this channel"

        else:
            reply = "This is not a channel I can subscribe to"

        self.api.create_message(channel_id, data={'text': reply})

    def cmd_part(self, channel_id, pnut_user, args):
        reply = ""
        
        channel, meta = self.api.get_channel(args)

        if int(pnut_user['id']) == channel.owner.id or int(pnut_user['id']) in self.config['admins']:

            if channel.type == 'io.pnut.core.chat':

                if channel.you_subscribed:

                    sc, sm = self.api.unsubscribe_channel(args)
                    if not sc.you_subscribed:
                        reply = "I am no longer subscribed to this channel"

                else:
                    reply = "I am currently not subscribed to this channel"

            else:
                reply = "This is not a channel I can subscribe to"

        else:
            reply = "ಠ_ಠ"

        self.api.create_message(channel_id, data={'text': reply})

    def cmd_subscribed(self, channel_id, pnut_user):
        reply = ""
        
        if int(pnut_user['id']) in self.config['admins']:

            channels, meta = self.api.subscribed_channels()
            for c in channels:
                if c.type == 'io.pnut.core.chat':
                    reply += "{0}, ".format(c.id)

        else:
            reply = "ಠ_ಠ"

        self.api.create_message(channel_id, data={'text': reply})

    def cmd_get_webhook(self, channel_id, pnut_user, args):
        reply = ""
        
        channel, meta = self.api.get_channel(args)

        if int(pnut_user['id']) == channel.owner.id:
            try:
                botchannel = self.session.query(BotChannel).filter(BotChannel.channel_id == int(channel.id)).one()
                if not botchannel.secret:
                    botchannel.secret = ''.join(secrets.choice(string.ascii_letters + string.digits) for i in range(14))
                    self.session.commit()
                
                reply = "Webhook URL: {0}/{1}\n".format(self.config['webhook_url'], channel.id)
                reply += "Secret: {0}".format(botchannel.secret)
            except NoResultFound:
                reply = "I am currently not subscribed to this channel.\n"
                reply += "Use the 'join' command to add me first."
            except Exception as e:
                logging.exception('get_webhook')

        else:
            reply = "ಠ_ಠ"

        self.api.create_message(channel_id, data={'text': reply})

    # create <project> <title>
    def cmd_create(self, channel_id, token, text):
        argv = text.split(' ', 1)

        if len(argv) < 2:
            reply = "usage: create <project> <title>"

        else:
            project = argv[0]
            details = argv[1].split('\n', 1)
            title = details[0]
            if len(details) > 1:
                description = details[1]
            else:
                description = None

            gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
            glproject = gl.projects.get(project)
            issue = glproject.issues.create({'title': title, 'description': description})

            reply = "Issue [#{0}]({1}) has been created.".format(issue.iid, issue.web_url)

        self.api.create_message(channel_id, data={'text': reply})

    # close <project> <id>
    def cmd_close(self, channel_id, token, text):
        argv = text.split(' ', 1)

        if len(argv) < 2:
            reply = "usage: close <project> <id>"

        else:
            project = argv[0]
            iid = argv[1]

            gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
            glproject = gl.projects.get(project)
            issue = glproject.issues.get(iid)
            issue.state_event = 'close'
            issue.save()

            reply = "Issue [#{0}]({1}) has been closed.".format(issue.iid, issue.web_url)

        self.api.create_message(channel_id, data={'text': reply})

    # comment <project> <id> <comment>
    def cmd_comment(self, channel_id, token, text):
        argv = text.split(' ', 2)

        if len(argv) < 3:
            reply = "usage: close <project> <id> <comment>"

        else:
            project = argv[0]
            iid = argv[1]
            comment = argv[2]

            gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
            glproject = gl.projects.get(project)
            issue = glproject.issues.get(iid)
            note = issue.notes.create({'body': comment})

            reply = "Comment has been created for issue [#{0}]({1}).".format(issue.iid, issue.web_url)

        self.api.create_message(channel_id, data={'text': reply})

    def get_subscribed(self):
        channels, meta = self.api.subscribed_channels()
        botchannels = self.session.query(BotChannel).all()

        # remove any unsubscribbed channels
        for bc in botchannels:
            if bc.channel_id not in [int(chan.id) for chan in channels]:
                self.session.delete(bc)
        self.session.commit()

        for chan in channels:

            if chan.type == 'io.pnut.core.chat' or chan.type == 'io.pnut.core.pm':

                if int(chan.id) in [bc.channel_id for bc in botchannels]:
                    channel = self.session.query(BotChannel).filter(BotChannel.channel_id == int(chan.id)).one()
                    channel.channel_owner = chan.owner.id
                    channel.channel_admins = json.dumps(chan.acl.full.user_ids)

                else:
                    channel = BotChannel(
                        channel_id=int(chan.id),
                        channel_type=chan.type,
                        channel_owner=chan.owner.id,
                        channel_admins=json.dumps(chan.acl.full.user_ids))
                    self.session.add(channel)

                self.session.commit()

    def subscription(self, channel_id):
        channel, meta = self.api.get_channel(channel_id)

        if channel.you_subscribed:
            newchannel = BotChannel(
                channel_id=int(channel.id),
                channel_type=channel.type,
                channel_owner=channel.owner.id,
                channel_admins=json.dumps(channel.acl.full.user_ids))
            self.session.add(newchannel)
        else:
            try:
                oldchannel = self.session.query(BotChannel).filter(BotChannel.channel_id == int(channel.id)).one()
                self.session.delete(oldchannel)
            except NoResultFound:
                return

        try:
            self.session.commit()
        except Exception:
            self.ession.rollback()
            return


def main():

    a_parser = argparse.ArgumentParser()
    a_parser.add_argument(
        '-d', action='store_true', dest='debug',
        help="debug logging"
    )
    a_parser.add_argument(
        '-c', '--config', default="config.yaml",
        help="configuration file"
    )
    a_parser.add_argument(
        '-s', '--store', default="store.db",
        help="database store url"
    )
    args = a_parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if SNAP_DATA is None:
        filename = args.config
        db_url = args.store
    else:
        filename = SNAP_DATA + '/config.yaml'
        db_url = 'sqlite:///' + SNAP_DATA + '/store.db'

    with open(filename, 'rb') as config_file:
        config = yaml.load(config_file, Loader=yaml.SafeLoader)

    # setup the database connection
    engine = create_engine(db_url)
    Base.metadata.create_all(engine)
    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()

    bot = PnutBot(config, session)

    while True:
        running = bot.ws.run_forever()
        if not running:
            break
        time.sleep(30)
    bot.ws.close()

if __name__ == "__main__":
    main()
