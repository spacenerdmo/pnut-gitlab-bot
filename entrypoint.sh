#!/bin/sh

if [ -z ${WORKERS+x} ]; then
    WORKERS=2;
fi

if [ -z ${LOGLEVEL+x} ]; then
    LOGLEVEL='debug';
fi

if [ ${LOGLEVEL} = "debug" ]; then
	DEBUG='-d';
fi

SNAP_DATA=/data gunicorn -b 0.0.0.0:4004 -w ${WORKERS} --log-level ${LOGLEVEL} --log-file /data/gunicorn.log pnut_gitlab.webhook:app -D
status=$?
if [ $status -ne 0 ]; then
    echo "Failed to start gunicorn: $status"
    exit $status
fi

exec pnut-gitlab-bot ${DEBUG} -c /data/config.yaml -s sqlite:////data/store.db
